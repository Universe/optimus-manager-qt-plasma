# ![Screenshot](https://gitea.artixlinux.org/Universe/optimus-manager-qt/raw/branch/master/logos/48-apps-optimus-manager.png) `Optimus Manager Qt` for Artix Linux

[![GitHub (pre-)release](https://img.shields.io/github/release/Shatur95/optimus-manager-qt/all.svg)](https://github.com/Shatur95/optimus-manager-qt/releases)
[![Codacy grade](https://img.shields.io/codacy/grade/5cd6782b094743eaa6fe35bc20650914.svg)](https://app.codacy.com/project/Shatur95/optimus-manager-qt/dashboard)

**Optimus Manager Qt** is an interface for **Optimus Manager** that allows to configure and switch GPUs on Optimus laptops using the tray menu.

**Note:** Please send all non-interface related issues to [Optimus Manager](https://github.com/Askannz/optimus-manager) repository.

## Content

-   [Screenshots](#screenshots)
-   [Features](#features)
-   [Third-party](#third-party)
-   [Localization](#localization)

## Screenshots

<p align="center">
  <img src="https://raw.githubusercontent.com/Shatur95/optimus-manager-qt/master/screenshots/tray.png" alt="Tray screenshot"/>
  <img src="https://raw.githubusercontent.com/Shatur95/optimus-manager-qt/master/screenshots/tray-menu.png" alt="Tray menu screenshot"/>
</p>
<p align="center">
  <img src="https://raw.githubusercontent.com/Shatur95/optimus-manager-qt/master/screenshots/settings-general.png" width="392px" height="300px" alt="General settings screenshot"/>
  <img src="https://raw.githubusercontent.com/Shatur95/optimus-manager-qt/master/screenshots/settings-optimus.png" width="392px" height="300px" alt="Optimus settings screenshot"/>
</p>

## Features

-   Display the current video card in the system tray
-   Graphic menu for Optimus Manager configuration
-   Switching video cards using the context menu
-   Customizable tray icon
-   Optional extended Plasma support (you can compile it with or without Plasma dependencies, see below)

## Third-party

### Libraries

This project uses [SingleApplication](https://github.com/itay-grudev/SingleApplication).

### Icons

[circle-flags](https://github.com/HatScripts/circle-flags "A collection of 300+ minimal circular SVG country flags") icons are used for flags.

Nvidia icon from the [Masalla icon theme](https://github.com/masalla-art/masalla-icon-theme "Icon theme for UNIX-like OS") is used for the application and status icons.

## Localization

To help with localization you can use [Crowdin](https://crowdin.com/project/optimus-manager-qt) or translate files in `data/translations` with [Qt Linguist](https://doc.qt.io/Qt-5/linguist-translators.html) directly. To add a new language, write to the original developer on the Crowdin project page or copy `data/translations/optimus-manager.ts` to `data/translations/optimus-manager_<ISO 639-1 language code>.ts`, translate it and send a pull request.

## Packages for Artix Linux

`optimus-manager-qt-plasma` for plasma desktop
